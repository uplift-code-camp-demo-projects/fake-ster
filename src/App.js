import React, { Component } from 'react';
import './App.css';
import TopBar from './TopBar/TopBar';
import UserInput from './UserInput/UserInput';
import FriendList from './FriendList/FriendList';
import Post from './Post/Post';

class App extends Component {
  state = {
    posts: [
      {
        user: 'Kevin Dimadapa',
        post:
          "I've learned that people will forget what you said, people will forget what you did, but people will never forget how you made them feel. -Maya Angelou",
      },
      {
        user: 'Beatrice Kasingkasing',
        post:
          'First, have a definite, clear practical ideal; a goal, an objective. Second, have the necessary means to achieve your ends; wisdom, money, materials, and methods. Third, adjust all your means to that end." -Aristotle',
      },
    ],
    friends: [
      {
        name: 'Neil Mapayapa',
      },
      {
        name: 'Kathryn Bagongligo',
      },
      {
        name: 'Justin Alapaap',
      },
    ],
  };

  userHandler = (event) => {
    this.setState({
      posts: [
        {
          user: event.target.value,
          post:
            "I've learned that people will forget what you said, people will forget what you did, but people will never forget how you made them feel. -Maya Angelou",
        },
        {
          user: 'Beatrice Kasingkasing',
          post:
            'First, have a definite, clear practical ideal; a goal, an objective. Second, have the necessary means to achieve your ends; wisdom, money, materials, and methods. Third, adjust all your means to that end." -Aristotle',
        },
      ],
    });
  }

  postHandler = (event) =>{
    this.setState({
      posts: [
        {
          user: 'Kevin Dimadapa',
          post: event.target.value,
        },
        {
          user: 'Beatrice Kasingkasing',
          post:
            'First, have a definite, clear practical ideal; a goal, an objective. Second, have the necessary means to achieve your ends; wisdom, money, materials, and methods. Third, adjust all your means to that end." -Aristotle',
        },
      ],
    });
    
  }

  topFriendHandler = (event) => {
    this.setState({
      friends: [
        {
          name: event.target.value,
        },
        {
          name: 'Kathryn Bagongligo',
        },
        {
          name: 'Justin Alapaap',
        },
      ],
    });
  }

  render() {
    return (
      <div className="App">
        <TopBar />
        <UserInput
          label="User:"
          currentName={this.state.posts[0].user}
          changed={this.userHandler}
        />
        <UserInput 
          label="What's on your Mind?:" 
          currentName={this.state.posts[0].post} 
          changed={this.postHandler} />
        <Post user={this.state.posts[0].user} post={this.state.posts[0].post} />
        <Post user={this.state.posts[1].user} post={this.state.posts[1].post} />
        <UserInput 
          label="Top Friend:" 
          changed={this.topFriendHandler}/>
        <FriendList friends={this.state.friends} />
      </div>
    );
  }
}

export default App;
